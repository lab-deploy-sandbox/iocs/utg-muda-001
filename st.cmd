# -----------------------------------------------------------------------------
# muda startup cmd
# -----------------------------------------------------------------------------
require ecmccfg 6.3.2
require momuda
require essioc

# -----------------------------------------------------------------------------
# setting parameters when not using auto deployment
# -----------------------------------------------------------------------------
epicsEnvSet(P, "utg-muda-001")
epicsEnvSet(R, ":")
epicsEnvSet(IOCNAME, "utg-muda-001")

# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
#
iocshLoad("$(essioc_DIR)common_config.iocsh")
# muda
iocshLoad("$(momuda_DIR)/momuda.iocsh")
# ecmc
iocshLoad("$(momuda_DIR)/ecmc.iocsh")
# EOF
